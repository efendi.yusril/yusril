import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import {a,ab,c,cd,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t} from '../assets';

const Bawah = () => {
    return (
        <View style={{position: 'absolute', bottom: 100}}>
            <View style={{width: 400, height: 53, flexDirection: 'row', backgroundColor: 'white', borderBottomWidth: 5, borderBottomColor: 'black'}}>
                <Image source={k} style={styles.k} />
                <Image source={l} style={styles.l} />
                <Image source={m} style={styles.m} />
                <Image source={c} style={styles.c} />
                <Image source={a} style={styles.a} />
            </View>
        </View>
    );
};

export default Bawah;

const styles = StyleSheet.create({
    k: {
        marginTop: 9,
        marginLeft:15,
        width: 25,
        height:25,
    },

    l: {
        marginTop: 9,
        marginLeft:52,
        width: 25,
        height:25,
    },

    m: {
        marginTop: 9,
        marginLeft:52,
        width: 25,
        height:25,
    },

    c: {
        marginTop: 9,
        marginLeft:52,
        width: 22,
        height:25,
    },

    a: {
        marginTop: 9,
        marginLeft:52,
        width: 28,
        height:28,
        borderRadius: 28/2,
    },
});
