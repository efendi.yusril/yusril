import React from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import {a,ab,c,cd,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t} from '../assets';

const Props = props => {
    return (
        <View>
            <Image source={props.abc} style={styles.abc}/>
        </View>
    );
};

const Cerita = () => {
    return (
        <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
                <Props abc={a} />
                <Props abc={r} />
                <Props abc={s} />
                <Props abc={t} />
            </View>
            <View>
                <Text style={styles.tex}>Cerita Anda</Text>
            </View>
        </View>
    );
};

export default Cerita;

const styles = StyleSheet.create({
    abc: {
        marginTop: 25,
        marginLeft:18,
        width: 70,
        height:70,
        borderRadius:70/2,

    },

    tex: {
        marginLeft:16,
        color: 'black',

    },
});
