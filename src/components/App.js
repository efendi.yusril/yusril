import React from 'react';
import { View, ScrollView } from 'react-native';
import { Atas, Cerita, Isi, Bawah } from '../components';

const App = () => {
  return (
    <View style={{backgroundColor: 'white'}}>
      <View>
        <Atas />
        <ScrollView>
          <Cerita />
          <Isi />
          <Isi />
          <Isi />
          <Isi />
        </ScrollView>
        <Bawah />
      </View>
    </View>
  );
};

export default App;
