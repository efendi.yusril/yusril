import React, { useState } from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import {a,ab,c,cd,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t} from '../assets';

const Isi = () => {
    const [plus, setPlus] = useState(0);
    return (
        <View>
            <View style={{flexDirection: 'row'}}>
                <Image source={a} style={styles.a}/>
            </View>
            <Image source={p} style={styles.p}/>
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => setPlus(plus + 1)}>
                    <Image source={e} style={styles.e}/>
                </TouchableOpacity>
                <Image source={f} style={styles.f}/>
                <Image source={g} style={styles.g}/>
                <Image source={i} style={styles.i}/>
            </View>
            <Text style={styles.tex}>{plus}</Text>
        </View>
    );
};

export default Isi;

const styles = StyleSheet.create({
    a: {
        marginTop: 10,
        marginLeft:10,
        width: 30,
        height:30,
        borderRadius:30/2,

    },

    p: {
        marginTop: 10,
        width: 370,
        height:370,

    },

    e: {
        marginTop: 10,
        marginLeft:10,
        width: 28,
        height:25,

    },

    f: {
        marginTop: 10,
        marginLeft:10,
        width: 25,
        height:25,

    },

    g: {
        marginTop: 10,
        marginLeft:10,
        width: 28,
        height:25,

    },

    i: {
        marginTop: 10,
        marginLeft:215,
        width: 21,
        height:25,

    },

    tex: {
        marginTop: 10,
        marginLeft:10,
        marginBottom: 10,
        color: 'black',

    },

});