import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import {a,ab,c,cd,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t} from '../assets';
const Atas = () => {
    return (
        <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1, height: 53, flexDirection: 'row', backgroundColor: 'white'}}>
                <Image source={d} style={styles.d} />
                <Image source={e} style={styles.e} />
                <Image source={g} style={styles.g} />
            </View>
        </View>
    );
};

export default Atas;

const styles = StyleSheet.create({
    d: {
        marginTop: 14,
        marginLeft:18,
        width: 106,
        height:30,
    },

    e: {
        marginTop:14,
        marginLeft: 140,
        width: 29,
        height: 26,
    },

    g: {
        marginTop: 16,
        marginLeft: 25,
        width: 29,
        height: 25,
    },
});
