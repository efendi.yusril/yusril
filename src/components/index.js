import Atas from './Atas';
import Cerita from './Cerita';
import Isi from './Isi';
import Bawah from './Bawah';

export { Atas, Cerita, Isi, Bawah };